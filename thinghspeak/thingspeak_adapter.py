import json
import string
import datetime
import time
import datetime
import numpy as np
import sys
import requests
sys.path.append("../mqtt/")
from MQTT_classes import Subscriber

#receive messages from mqtt and translate to rest to send to thinghspeak
class ThingspeakAdaptor(Subscriber):
	def __init__(self):
		fp=open('../system/config.JSON','r')
		self.conf=json.load(fp)
		fp.close()
		# loading the address of the Catalog
		self.catalog_ip=self.conf['catalog']['ip']
		self.catalog_port=self.conf['catalog']['port']

		if self.conf['registered']['flag']==1:
			self.admin_email=self.conf['admin']['email']         
			self.admin_password=self.conf['admin']['password']
			# catalog acess information about the Main box name to get info about the sensors
			self.catalog_url='http://'+str(self.catalog_ip)+':'+str(self.catalog_port)	
			# catalog acess information about the Main box name to get info about the sensors
			self.mainb = requests.get(str(self.catalog_url +'/mainbox_name/'+str(self.admin_email)+"/"+str(self.admin_password)))
			if (self.mainb.status_code == 200):
				self.mainb_obj = self.mainb.json()
			self.name=self.mainb_obj['name']
			self.appid=self.mainb_obj['key']
			id_string = 'Thinghspeak/'+ str(self.name)
			self.catalog_url='http://'+str(self.catalog_ip)+':'+str(self.catalog_port)
			self.subscribed_topic= 'Thinghspeak/#'
			print (self.subscribed_topic)			
			super(ThingspeakAdaptor,self).__init__(clientID=id_string,sub_topic=self.subscribed_topic)
		else:
			pass

	def writeMeasurements(self,mensagem):
		Thinkmessage=json.loads(mensagem)
		if len(Thinkmessage)==3:
			print("-->Received message",Thinkmessage)
			Thinkmessage_Temperature=str(Thinkmessage["Temperature"])
			Thinkmessage_Humidity=str(Thinkmessage["Humidity"])
			Thinkmessage_mood=str(Thinkmessage["mood"])
			Thinghspeak_mess = requests.get(str(self.catalog_url +'/get_thinghspeak/'+str(self.admin_email)+"/"+str(self.admin_password)))
			if (Thinghspeak_mess.status_code == 200):
				raw_dict=Thinghspeak_mess.json()
				api_key=raw_dict["api_key"]
				url=raw_dict["url"]
				channel=raw_dict['channel_id']
			moods =json.dumps({"Disgust":1,"Scared":2,"Happy":3,"Sad":4,"Surprised":5,"Neutral":6,"Angry":7})
			moods = json.loads(moods)
			self.update_thinghspeak(str(moods[Thinkmessage_mood]),str(Thinkmessage_Temperature),str(Thinkmessage_Humidity),str(api_key),str(url),str(channel))
		else:
			pass
	def update_thinghspeak(self,mood,temperature,humidity,api_key,url,channel):
		print("----Uploading the Thinghspeak----")
		timenow = (datetime.datetime.today().strftime("%Y-%m-%dT%H:%M:%SZ"))
		urlb=url.format(str(api_key),mood,temperature,humidity)
		rw=requests.get(urlb)

	def mqtt_onMessageReceived(self, paho_mqtt, userdata, msg):
		self.writeMeasurements((msg.payload))

if __name__ == '__main__':	
	ta=ThingspeakAdaptor()
	ta.mqtt_start()
	time.sleep(3600)
	ta.mqtt_stop()
