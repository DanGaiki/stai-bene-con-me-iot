import json
import time
from cherrypy import config, engine, tree
import cherrypy
import os, os.path



class WebService(object):

	exposed=True
	def GET (self,*uri):
		if uri[0]=='green': # can retrieve all or device ID

			return	"""<html>
						<head>
						<title>
						webpage
						</title>
						</head>
						<h2>I'am Happy!</h2>
						<body style="background-color:green;text-align:center">
						<img src="img/happy.jpg" alt="Happy" width="500" height="500">
						</body>
						</html>"""

		if uri[0]=='blue':
			return	"""<html>
						<head>
						<title>
						webpage
						</title>
						</head>
						<h2>I'am Angry!</h2>
						<body style="background-color:blue;text-align:center">
						<img src="img/angry.jpg" alt="Angry" width="500" height="500">
						</body>
						</html>"""

		if uri[0]=='red':
			return	"""<html>
						<head>
						<title>
						webpage
						</title>
						</head>
						<h2>I'am Neutral!</h2>
						<body style="background-color:red;text-align:center">
						<img src="img/neutral.jpg" alt="Neutral" width="500" height="500">
						</body>
						</html>"""

		if uri[0]=='yellow':
			return	"""<html>
						<head>
						<title>
						webpage
						</title>
						</head>
						<h2>I'am Sad!</h2>
						<body style="background-color:yellow;text-align:center">
						<img src="img/sad.jpg" alt="Sad" width="500" height="500">
						</body>
						</html>"""

		else:
			raise cherrypy.HTTPError('404',' function NOT FOUND')

if __name__ == '__main__':
	conf = {
		'/': {
		'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
		'tools.sessions.on':True,
		'tools.staticdir.root': os.path.abspath(os.getcwd())},
		'/img': {
        'tools.staticdir.on': True,
        'tools.staticdir.dir': './img'},
		'global': {
        'environment': 'production',
        'log.screen': True,
        'server.socket_host': '0.0.0.0',
        'server.socket_port': 6062,
        'engine.autoreload_on': True,

		}
	}
	cherrypy.quickstart(WebService(), '/', conf)
	cherrypy.engine.start()
	cherrypy.engine.block()