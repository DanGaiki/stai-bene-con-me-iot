from telegram.ext import Updater,CommandHandler,Filters,MessageHandler
import logging
import configparser
import requests
import sys
import json
import dweepy
import time
import datetime
import sys
import string
import numpy as numpy
import json
import configparser as cfg
sys.path.append("../mqtt/")
from MQTT_classes import Subscriber

class Telegrambot(Subscriber):
	def __init__(self):
		fp=open('../system/config.JSON','r')
		self.conf=json.load(fp)
		fp.close()
		# loading the address of the Catalog
		self.catalog_ip=self.conf['catalog']['ip']
		self.catalog_port=self.conf['catalog']['port']
		self.token=self.conf['Telegram']['token']
		self.url=self.conf['Telegram']['url']
		# catalog acess information about the Main box name to get info about the sensors
		self.catalog_url='http://'+self.catalog_ip + ':' + self.catalog_port
		# catalog acess information about the Main box name to get info about the sensors
		id_string = 'Telegram/'
		self.subscribed_topic= 'Telegram/#'
		print (self.subscribed_topic)
		super(Telegrambot,self).__init__(clientID=id_string,sub_topic=self.subscribed_topic)
		self.updater=Updater(self.token, use_context=True)
		self.dispatcher = self.updater.dispatcher
		if self.conf['registered']['flag']==1:
			self.controller=1
			self.email=self.conf['admin']['email']
			self.password=self.conf['admin']['password']
			self.uid=self.conf['admin']['uid']
		else:
			self.controller=0
		#telegram handlers
		
		start_handler = CommandHandler('start', self.msg_start)
		self.dispatcher.add_handler(start_handler)
		
		new_user = CommandHandler('new_user', self.new_user, pass_args=True)
		self.dispatcher.add_handler(new_user)
		
		info_homemood = CommandHandler('info_homemood', self.info_homemood, pass_args=True)
		self.dispatcher.add_handler(info_homemood)
		
		info_lastmeasurement = CommandHandler('info_lastmeasurement', self.info_lastmeasurement, pass_args=True)
		self.dispatcher.add_handler(info_lastmeasurement)

		echo_handler = MessageHandler(Filters.text, self.msg_echo)
		self.dispatcher.add_handler(echo_handler)

# for managing errors
		logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',level=logging.INFO)
		self.job_queue=self.updater.job_queue


	def error_callback(bot, update, error):
		try:
			raise error
		except Unauthorized:
			logging.debug("TELEGRAM ERROR: Unauthorized - %s" % error)
		except BadRequest:
			logging.debug("TELEGRAM ERROR: Bad Request - %s" % error)
		except TimedOut:
			logging.debug("TELEGRAM ERROR: Slow connection problem - %s" % error)
		except NetworkError:
			logging.debug("TELEGRAM ERROR: Other connection problems - %s" % error)
		except ChatMigrated as e:
			logging.debug("TELEGRAM ERROR: Chat ID migrated?! - %s" % error)
		except TelegramError:
			logging.debug("TELEGRAM ERROR: Other error - %s" % error)
		except:
			logging.debug("TELEGRAM ERROR: Unknown - %s" % error) 


	def start_polling(self):
		# to start the bot
		self.updater.start_polling()
		print ("Bot started polling.")
		# Run the bot until you press Ctrl-C or the process receives SIGINT,
		# SIGTERM or SIGABRT. This should be used most of the time, since
		# start_polling() is non-blocking and will stop the bot gracefully.
		self.updater.idle()

	def msg_start(self,update, context):
		self.chat_id=update.effective_chat.id
		if self.controller==0:
			context.bot.send_message(chat_id=self.chat_id, text="Dear user, you are not ready to use the system. Please, add the new user! Type the /new_user email password")
		else:
			context.bot.send_message(chat_id=self.chat_id, text="Dear user, you are ready to use the system!")

	def msg_echo(self,update, context):
		context.bot.send_message(chat_id=self.chat_id, text='Sorry, this is not a command. All commands start with \'/\'')

	def new_user(self,update, context):
		#update.message.reply_text("You said: " + user_says)
		self.chat_id=update.effective_chat.id	
		print (str(self.catalog_url +'/register/'+str(context.args[0])+"/"+str(context.args[1])))	
		if len(context.args)==0:
			text='wrong number of arguments.'
		elif len(context.args)<2 and len(context.args)>2:
			text='wrong number of arguments.'
		else:
			actuator_mess = requests.get(str(self.catalog_url +'/register/'+str(context.args[0])+"/"+str(context.args[1])+"/"+str(self.chat_id)))
			if (actuator_mess.status_code == 200):
				print ("status code 200")
				context.bot.send_message(chat_id=self.chat_id, text="Dear new user, you are ready to use the system, new user, main box and chat ID was registered!")
			
	def info_homemood(self,update, context):
		chat_id=update.effective_chat.id
		if self.controller==1:
			actuator_mess = requests.get(str(self.catalog_url +'/get_sensors_and_actuators/'+str(self.email)+"/"+str(self.password)))
			if (actuator_mess.status_code == 200):
				print ("status code 200")
				raw_dict=actuator_mess.json()
				objc=raw_dict["Actuators"]
				msg=""
				for key in objc.keys():
					msg=msg+(f"\n-Actuator {key}-\n")
					for par in objc[key].keys():
						msg=msg+(f"{str(par)}: {str(objc[key][par])}\n")
				update.message.reply_text("This is the current status of actuators\n" + msg)
		else:
			context.bot.send_message(chat_id=self.chat_id, text="Dear user, you are not ready to use the system. Please, the sensors and actuators and run the system first to ask about the system!")

	def info_lastmeasurement(self,update, context):
		chat_id=update.effective_chat.id
		if self.controller==1:
			update.message.reply_text("The last mood is " +self.Mood)
			update.message.reply_text("The last Temperature is " +self.Temperature +"C")
			update.message.reply_text("The last Humidity is " +self.Humidity +"%")
		else:
			context.bot.send_message(chat_id=self.chat_id, text="Dear user, you are not ready to use the system. Please, add the nsystem first!")

	def startup(self):
		self.mqtt_start()
		self.start_polling()

	def mqtt_onMessageReceived(self, paho_mqtt, userdata, msg):
		self.writeMeasurements((msg.payload))

	def writeMeasurements(self,mensagem):
		m=json.loads(mensagem)
		self.Humidity=m['Humidity']
		self.Temperature=m['Temperature']
		self.Mood=m['mood']
		print("Control message",self.Humidity,self.Temperature,self.Mood)

if __name__ == '__main__':
	bot=Telegrambot()
	bot.startup()
	time.sleep(3600)
	bot.mqtt_stop()
