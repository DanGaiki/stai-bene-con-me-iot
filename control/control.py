import json
import string
import datetime
import time
import datetime
import numpy as np
import sys
import requests
import subprocess
import webbrowser
sys.path.append("../mqtt/")
from MQTT_classes import PublisherSubscriber

class Colourcontrol(PublisherSubscriber):
	def __init__(self):
		fp=open('../system/config.JSON','r')
		self.conf=json.load(fp)
		fp.close()
		# loading the address of the Catalog
		self.catalog_ip=self.conf['catalog']['ip']
		self.catalog_port=self.conf['catalog']['port']
		self.fake_actuator_ip=self.conf['fake_actuator']['ip']
		self.fake_actuator_port=self.conf['fake_actuator']['port']
		# catalog acess information about the Main box name to get info about the sensors
		self.catalog_url='http://'+str(self.catalog_ip)+':'+str(self.catalog_port)
		self.fake_actuator_url='http://'+str(self.fake_actuator_ip)+':'+str(self.fake_actuator_port)
		if self.conf['registered']['flag']==1:
			self.controller=1
			self.admin_email=self.conf['admin']['email']         
			self.admin_password=self.conf['admin']['password']
			self.mainb = requests.get(str(self.catalog_url +'/mainbox_name/'+str(self.admin_email)+"/"+str(self.admin_password)))
			if (self.mainb.status_code == 200):
				self.mainb_obj = self.mainb.json()
				self.name=self.mainb_obj['name']
				self.appid=self.mainb_obj['key']
				#catalog acess informaton about the weather
				self.weath = requests.get(str(self.catalog_url +'/get_weather/'+str(self.admin_email)+"/"+str(self.admin_password)))
			if (self.mainb.status_code == 200):
				self.weath_obj=self.weath.json()
				self.appidw=self.weath_obj['appid']
				self.lat=self.weath_obj['lat']
				self.lon=self.weath_obj['lon']
				self.sunrise=self.weath_obj['sunrise']
				self.sunset=self.weath_obj['sunset']
				self.forecast_days=self.weath_obj['forecast_days']
				self.weather_url=self.weath_obj['weather_url'].format(self.lat,self.lon,self.appidw)
				self.forecast_url=self.weath_obj['forecast_url'].format(self.lat,self.lon,self.appidw)
				self.weather_interval=self.weath_obj['weather_interval']
				self.forecast_interval=self.weath_obj['forecast_interval']
				id_string = 'Controller/'+ str(self.name)
				#self.subscribed_topic=str(self.name) + '/#'
				self.subscribed_topic= 'Control/#'
				print("##Controller of actuators waiting messages from automatic Sensors ##")
				super(Colourcontrol,self).__init__(clientID=id_string,sub_topic=self.subscribed_topic)

		else:
			print("user not registered Yet")
			pass

	def completeMeasurement(self,mensagem):
		self.control=json.loads(mensagem)
		if len(self.control) ==2:		
			self.control_sensor=str(self.control["sensor"])
			self.control_value=str(self.control["value"])
		else:
			self.sensor_mood=str(self.control["sensor"])
			self.mood=(self.control["mood"])
			self.moodlist=['Scared','Sad','Neutral','Disgust','Angry', 'Surprised','Happy']
			for i in range(len(self.mood)):
				if self.mood[self.moodlist[i]]==1:
					self.moodstatus=str(self.moodlist[i])
			self.user=str(self.control["username"])
			print("---Control action---")
			print("Sensor",self.sensor_mood)
			print("Received control value =", self.control_value)
			print("Classificated mood",self.moodstatus)
			print("Identified User",self.user)

			if (self.control_sensor == self.sensor_mood):
				self.colorchanges(self.control_sensor,self.control_value,self.moodstatus,self.user)

	def colorchanges(self,sensor,value, mood, user):
		#colecting the sensors resouces	
		print("initializing control changes if sensor in automatic")
		sensors_res = requests.get(str(self.catalog_url +'/get_sensors_and_actuators/'+str(self.admin_email)+"/"+str(self.admin_password)))
		if (sensors_res.status_code == 200):
			raw_dict=sensors_res.json()
		#sensors list
		timenow = (datetime.datetime.today().strftime("%Y-%m-%d %H:%M:%S"))
		actuatorslist=[]
		actuatorslistb=[]

		#check if the sensor exist and if is in automatic mode
		for i in range(len(raw_dict["Sensors"])):
			if (raw_dict["Sensors"]["S"+ str(i+1)]["status"]) == "automatic" :
				if str(sensor) == ("S"+ str(i+1)) and value =="on":
					print ("the mood is : ",str(mood))
					if (str(mood)=="Happy"):
						color="green"
						print("selected color green")
					if (str(mood)=="Angry"):
						color="blue"
						print("selected color blue")
					if (str(mood)=="Neutral"): 
						color="red"
						print("selected color red")
					if (str(mood)=="Sad"):
						color="yellow"
						print("selected color yellow")
					if (str(mood)=="Scared"): 
						color="blue"
						print("selected color blue")
					if (str(mood)=="Surprised"):
						color="blue"
						print("selected color blue")

					#checking the place of this sensor
					place = raw_dict["Sensors"]["S"+ str(i+1)]["place"]
					#Checking the actuators for this  place
					for i in range(len(raw_dict["Actuators"])):
						if (raw_dict["Actuators"]["L"+ str(i+1)]["place"]) == str(place):
							actuatorslist.append(str("L"+ str(i+1)))
					control_message=({'Sensor':("S"+ str(i+1)) ,'color':color, 'time': timenow,'actuatorslist':actuatorslist,'command':'on'})
					print("control message sended to",control_message)
					p = subprocess.Popen(["firefox", "http://0.0.0.0:6062/"+str(color)])
					time.sleep(10) 
					p.kill()
					message=json.dumps(control_message)
					reply_topic='Actuators/'+topic.split('/')[1]
					#print(control_message)
					self.mqtt_publish(topic=reply_topic, message=message)

				if str(sensor) == ("S"+ str(i+1)) and value =="off":
					place = raw_dict["Sensors"]["S"+ str(i+1)]["place"]
					for i in range(len(raw_dict["Actuators"])):
						if (raw_dict["Actuators"]["L"+ str(i+1)]["place"]) == str(place):
							actuatorslistb.append(str("L"+ str(i+1)))
					control_messageb=({'Sensor':("S"+ str(i+1)) ,'color':'same', 'time': timenow,'actuatorslist':actuatorslistb,'command':'off'})
					message=json.dumps(control_messageb)
					reply_topic='Actuators/'+topic.split('/')[1]
					self.mqtt_publish(topic=reply_topic, message=message)					
			else:
				pass

	def mqtt_onMessageReceived(self, paho_mqtt, userdata, msg):
		self.completeMeasurement((msg.payload))

if __name__ == '__main__':
	Cc = Colourcontrol()
	#if Cc==True:
	Cc.mqtt_start()
	time.sleep(360)
	Cc.mqtt_stop()
	#else:
		#print("user not registerd Yet")
		#Cc = Colourcontrol()
