import pyrebase
import json
import re
import datetime
import os
import sys

class Firebase(object):

	def __init__(self, clientemail,passw):
#manage connection to broker
		with open('../system/keybase.json', 'r') as f:
			self.distros_dict = json.load(f)
#JSON DATA
		self.clientemail = clientemail
		self.passw = passw
		self.apiKey=(str(self.distros_dict["apiKey"]))
		self.authDomain=(str(self.distros_dict["authDomain"]))
		self.databaseURL=(str(self.distros_dict["databaseURL"]))
		self.projectId=(str(self.distros_dict["projectId"]))
		self.storageBucket=(str(self.distros_dict["storageBucket"]))
		self.messagingSenderId=(str(self.distros_dict["messagingSenderId"]))
		self.measurementId=(str(self.distros_dict["measurementId"]))
		self.firebase = pyrebase.initialize_app(self.distros_dict)
		self.auth = self.firebase.auth()
#authenticating 
	def aut(self,email,passw):
		user = self.auth.sign_in_with_email_and_password(email,passw)
		db = self.firebase.database()
		st= self.firebase.storage()
		return user,db,st
#Creating users
	def create_user_with_email_and_password(self,email, password):
		create = self.auth.create_user_with_email_and_password(email, password)
		return create
#Get account information
	def get_account_info(self,info):
		infoaccount = self.auth.get_account_info(info)
		return infoaccount
#Get email information
	def send_email_verification(self,info):
		infoemail = self.send_email_verification(info)
		return infoemail
#refresh tokens
	def refresh(self,info):
		user = self.auth.refresh(info)
		return user
#new database	
	def setdataamb(self,db,uid,data):
		print("setting a new House",uid)
		db.child("Houses").child(uid).set(data)
#add on catalog the main user
	def addmain(self,db,uid,data):
		db.child("Houses").child(uid).child("admin").set(data)
#loggeduser
	def setloggeduser(self,db,uid,data):
		db.child("Houses").child(uid).child("logged").set(data)
#add a MainBoxName
	def add_mainboxname(self,db,uid,data):
		print ("adding main box name",data)
		db.child("Houses").child(uid).child("mainbox").set(data)
#mood data on firebase with timestamp
	def mood(self,db,uid,datetime,data):
		db.child("Houses").child(uid).child("ambient").child("mood_database").child(datetime).update(data)
#user mood on database
	def usermood(self,db,uid,datetime,data):
		db.child("Houses").child(uid).child("ambient").child("housemood").child(datetime).set(data)
#remove data
	def removedata(self,db,uid,user,data):
		db.child("Houses").child(uid).child("users").child("mood").child(user).child(str(datetime.datetime.now())).set(data)
#update control data from ambient control
	def updatedata_actuator(self,db,uid,actuator_name,key,value):
		db.child("Houses").child(uid).child("ambient").child("actuators").child(actuator_name).update({key:value})
#setup how many days of forecast need to be exposed
	def set_forecast_days(self,db,uid,days):
		db.child("Houses").child(uid).child("ambient").child("settings").child("forecast_days").set(days)
#insert a new user on the database this user is not present on the authentication of firebase
	def new_user(self,db,uid,data):
		db.child("Houses").child(uid).child("users").set(data)
#insert a new actuator in the firebase
	def new_actuator(self,db,uid,data):
		db.child("Houses").child(uid).child("ambient").child("actuators").update(data)
#insert a new sensor on the firebase
	def new_sensor(self,db,uid,data):
		db.child("Houses").child(uid).child("ambient").child("sensors").update(data)
#insert a new room on the firebase
	def new_room(self,db,uid,data):
		db.child("Houses").child(uid).child("ambient_control").update(data)
#insert a new day on the control scheme
	def new_day(self,db,uid,room,data):
		db.child("Houses").child(uid).child("ambient_control").child(room).update(data)
#insert a new event inside a day on the firebase
	def new_event(self,db,uid,room,day,data):
		db.child("Houses").child(uid).child("ambient_control").child(room).child(day).update(data)
#change a actuator data inside actuators on firebase
	def update_actuator(self,db,uid,name,data):
		db.child("Houses").child(uid).child("ambient").child("actuators").child(name).update(data)
#change a sensor data inside a sensors in firebase
	def update_sensor(self,db,uid,name,data):
		db.child("Houses").child(uid).child("ambient").child("sensors").child(name).update(data)
#remove an actuator from the actuators list in firebase
	def remove_actuator(self, db,uid, name):
		db.child("Houses").child(uid).child("ambient").child("actuators").child(name).remove()
#remove an sensor from the sensors list in firebase
	def remove_sensor(self, db,uid, name):
		db.child("Houses").child(uid).child("ambient").child("sensors").child(name).remove()
#remove an event from the room, day 
	def remove_event(self, db,uid, room, day, name):
		db.child("Houses").child(uid).child("ambient_control").child(room).child(day).child(name).remove()
#remove some user from the users list
	def remove_user(self, db,uid, data):
		db.child("Houses").child(uid).child("users").set(data)
#update the last weather measured
	def updatelast_weather(self,db,uid,timenow,data):
		db.child("Houses").child(uid).child("ambient").child("weather").child(timenow).update(data)
#update the sunsrise and sunset time in the houseconf 
	def updatelast_sunsetsunrise(self,db,uid,datasun):
		db.child("Houses").child(uid).child("ambient").child("houseconf").update(datasun)
#update the last forecast data  
	def updatelast_forecast(self,db,uid,data):
		db.child("Houses").child(uid).child("ambient").child("weather_forecast").update(data)
#update the last forecast data  
	def add_chat_id(self,db,uid,data):
		db.child("Houses").child(uid).child("system").child(0).update(data)

#Storage
#set data on the storage
	def setdatastorage(self,st,filename,databrowse):
		st.child(filename).put(databrowse)
#The download method takes the path to the saved database file and the name you want the downloaded file to have.
	def downloaddatastorage(self,st,databrowse):
		st.download(databrowse)
#The get_url method takes the path to the saved database file and returns the storage url.
	def getdatastorage(self,st,databrowse):
		return st.child(databrowse).get_url(databrowse)
