import requests
import cherrypy
import json
import random
import string
import sys
import pyrebase
import time
import datetime
sys.path.append("../firebase/")
from fireclass import Firebase


class AMBIENT_Catalog(object):
	exposed=True
	def __init__(self):
		self.AMBIENT_file='../system/AMBIENT.JSON'
		self.ADMIN_file='../system/ADMIN.JSON'
		self.INIT_file='../system/INIT1.JSON'
		self.CONFIG_file='../system/config.JSON'
		fp=open('../system/config.JSON','r')
		self.conf=json.load(fp)
		fp.close()
		if self.conf['registered']['flag']==1:
			self.controller=1
			self.email=self.conf['admin']['email']
			self.password=self.conf['admin']['password']
			self.uid=self.conf['admin']['uid']
			self.fire=Firebase(self.email,self.password)
			self.usr,self.db,self.st=self.fire.aut(self.email,self.password)
		else:
			self.controller=0

	def update_DBjson(self):
		if self.controller==0:
			print("Before work with the catalog register the new user")
		else:
			self.uid=self.conf['admin']['uid']
			data = (self.db.get()).val()["Houses"][self.uid]
			datadump= json.dumps({self.uid : data })
			fc=open(self.AMBIENT_file,'w+')
			json.dump(json.loads(datadump),fc)
			fc.close()
			return data

	def register(self,email,passw,daat):
		ff=open(self.ADMIN_file,'r')
		ad=json.load(ff)
		ff.close()
		fire=Firebase(ad['email'],ad['pass'])
		usr,db,st=fire.aut(ad['email'],ad['pass'])
		#id=fire.create_user_with_email_and_password(email_reg,password_reg)
		id=fire.create_user_with_email_and_password(email,passw)
		print ("Created with id", id)
		#Update admin info on config.JSON
		self.userid=str(id['localId'])
		datadump=json.dumps({"admin":{"email":email,"password":passw,"uid":self.userid}})
		fc=open(self.CONFIG_file,'r')
		datanew=json.load(fc)
		fc.close()	
		datanew.update(json.loads(datadump))
		datanew['registered']['flag']=1
		fd=open(self.CONFIG_file,'w')
		json.dump(datanew,fd)
		fd.close()
		ini=open(self.INIT_file,'r')
		dt=json.load(ini)
		ini.close()
		usr,db,st=fire.aut(email,passw)
		fire.setdataamb(db,self.userid,dt)
		daat=json.dumps({"chat_id":daat})
		print(daat)
		fire.add_chat_id(db,self.userid,json.loads(daat))
		data = self.update_DBjson()
		idi=''.join(random.choice('0123456789abcdefghijklmnopqrstuvwyxz') for i in range(10))
		dat = json.dumps({'name':email,'key':idi})
		dat = json.loads(dat)
		fire.add_mainboxname(db,usr['localId'], dat)
		fc=open(self.AMBIENT_file,'w+')
		json.dump(data,fc)
		fc.close()
		data = self.update_DBjson()
		return "Added mainbox"

	def sign_up(self, NEW_USER):  #add a new user on the house
		if self.controller==0:
			print("Before work with the catalog register the new user")
		else:	
			data=self.update_DBjson()
		#authenticate with the home coordinator user
			user={"email":NEW_USER["email"], "image":NEW_USER["image"], "name":NEW_USER["name"],"storage":NEW_USER["storage"]}
			data["users"].append(user)
			self.fire.new_user(self.db,self.uid,data["users"])
			data=self.update_DBjson()

	def log_in(self, OLD_USER):			
		self.fire=Firebase(OLD_USER['email'],OLD_USER['pass'])
		self.usr,self.db,self.st=self.fire.aut(OLD_USER['email'],OLD_USER['pass'])
		dat = json.dumps({'email': OLD_USER['email']})
		dat = json.loads(dat)
		ts = int(time.time())
		dat = json.dumps({ts:{'email':OLD_USER['email']}})
		dat = json.loads(dat)
		self.fire.setloggeduser(self.db,self.uid,dat)
		#define logged user
		self.update_DBjson()

	def create_actuator(self, NEW_ACTUATOR):
		if self.controller==0:
			print("Before work with the catalog register the new user")
		else:	
			data=self.update_DBjson()		
			self.fire.new_actuator(self.db,self.uid, NEW_ACTUATOR)
			data = self.update_DBjson()
			return "created actuator"

	def create_sensor(self, NEW_SENSOR):
		if self.controller==0:
			print("Before work with the catalog register the new user")
		else:			
		#building a json for the firebase from the info given by the user
			data = self.update_DBjson()
			self.fire.new_sensor(self.db,self.uid, NEW_SENSOR)
			data = self.update_DBjson()
			return "created sensor"

	def createchange_event(self, NEW_EVENT):
		if self.controller==0:
			print("Before work with the catalog register the new user")
		else:	
			data = self.update_DBjson()
			key1=list(NEW_EVENT.keys())
			room=key1[0]
			key2=list(NEW_EVENT[room].keys())
			day=key2[0]
			key3=list(NEW_EVENT[room][day].keys())
			name=key3[0]
			if self.light_acceptance(NEW_EVENT[room][day][name]["color"], NEW_EVENT[room][day][name]["dimming"])=="accepted":
				if self.time_acceptance(day, NEW_EVENT[room][day][name]["start"], NEW_EVENT[room][day][name]["end"])=="accepted":
					if room in data[self.uid]["ambient_control"].keys():
						if day in data[self.uid]["ambient_control"][room].keys():
							self.fire.new_event(self.db,self.uid, room, day, NEW_EVENT[room][day])
						else:
							self.fire.new_day(self.db,self.uid, room, NEW_EVENT[room])
					else:
						self.fire.new_room(self.db,self.uid, NEW_EVENT)
					res="event created"
				else:
					res="day/time setting error"
			else:
				res="color/dimming not acceptable"
		
			data = self.update_DBjson()
			return res

	def change_actuator(self, NEW_ACTUATOR):
		if self.controller==0:
			print("Before work with the catalog register the new user")
		else:	
		#building a json for the firebase from the info given by the user
			data = self.update_DBjson()
			l=list(NEW_ACTUATOR.keys())
			new_data=NEW_ACTUATOR[l[0]]
			self.fire.update_actuator(self.db,self.uid, l[0], new_data)
			data = self.update_DBjson()
			return "inserted actuator"

	def change_sensor(self, NEW_SENSOR):
		if self.controller==0:
			print("Before work with the catalog register the new user")
		else:
		#building a json for the firebase from the info given by the user
			data = self.update_DBjson()
			l=list(NEW_SENSOR.keys())
			new_data=NEW_SENSOR[l[0]]
			self.fire.update_sensor(self.db,self.uid, l[0], new_data)
			data = self.update_DBjson()
			return "changed sensor"
	
	def delete_actuator(self, OLD_ACTUATOR):
		if self.controller==0:
			print("Before work with the catalog register the new user")
		else:		
			data = self.update_DBjson()
			if OLD_ACTUATOR in data[self.uid]["ambient"]["actuators"]:
				del data[self.uid]["ambient"]["actuators"][OLD_ACTUATOR]
				self.fire.remove_actuator(self.db,self.uid, OLD_ACTUATOR)
			fc=open(self.AMBIENT_file,'w+')
			json.dump(data,fc)
			fc.close()
			return "Deleted actuator"

	def delete_sensor(self, OLD_SENSOR):
		if self.controller==0:
			print("Before work with the catalog register the new user")
		else:
			data = self.update_DBjson()
			if OLD_SENSOR in data[self.uid]["ambient"]["sensors"]:
				del data[self.uid]["ambient"]["sensors"][OLD_SENSOR]
				self.fire.remove_sensor(self.db,self.uid, OLD_SENSOR)
			fc=open(self.AMBIENT_file,'w+')
			json.dump(data,fc)
			fc.close()
			return "Deleted sensor"

	def delete_event(self, ROOM,DAYWEEK,OLD_EVENT):
		if self.controller==0:
			print("Before work with the catalog register the new user")
		else:
			data = self.update_DBjson()
			if OLD_EVENT in data[self.uid]["ambient_control"][ROOM][DAYWEEK]:
				del data[self.uid]["ambient_control"][ROOM][DAYWEEK][OLD_EVENT]
				self.fire.remove_event(self.db,self.uid, ROOM, DAYWEEK, OLD_EVENT)
			fc=open(self.AMBIENT_file,'w+')
			json.dump(data,fc)
			fc.close()
			return "Deleted event"

	def delete_user(self, email):
		if self.controller==0:
			print("Before work with the catalog register the new user")
		else:
			data = self.update_DBjson()
			flag="user not found"
			max_i=len(data[self.uid]["users"])
			for i in range(0, max_i):
				if email==data[self.uid]["users"][i]["email"]:
					del data[self.uid]["users"][i]
					new_users=data[self.uid]["users"]
					self.fire.remove_user(self.db,self.uid, new_users)
					flag="user deleted"
			fc=open(self.AMBIENT_file,'w+')
			json.dump(data,fc)
			fc.close()
			return json.dumps(flag)

	def set_forecast_days(self, days):
		if self.controller==0:
			print("Before work with the catalog register the new user")
		else:
			data = self.update_DBjson()
			data[self.uid]['ambient']['settings']["forecast_days"]=days
			self.fire.set_forecast_days(self.db,self.uid, days)
			fc=open(self.AMBIENT_file,'w+')
			json.dump(data,fc)
			fc.close()
			return "Forecast set"

	def upload_forecast(self, FORECAST):     # raspmain upload forecast
		if self.controller==0:
			print("Before work with the catalog register the new user")
		else:
			data=self.update_DBjson()
			self.fire.updatelast_forecast(self.db,self.uid, FORECAST)
			data = self.update_DBjson()
			return "created actuator"

	def light_acceptance(self, COLOUR, INTpc):
		if self.controller==0:
			print("Before work with the catalog register the new user")
		else:
			INTENSITY=int(INTpc[:-1])
			colours=["red", "white", "green", "blue", "orange", "yellow"]
			if (COLOUR in colours) and (INTENSITY>-1) and (INTENSITY<101):
				return "accepted"
			else:
				return "error"

	def time_acceptance(self, DAY, START, END):
		if self.controller==0:
			print("Before work with the catalog register the new user")
		else:
			week=["Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday", "Sunday"]
			if DAY in week:
				if (int(START[:2])<25) and (int(START[3:5])<61) and (int(START[6:])<61):
					if (int(END[:2])<25) and (int(END[3:5])<61) and (int(END[6:])<61):
						return "accepted"
			else:
				return "error"

	def upload_mood(self,data):
		if self.controller==0:
			print("Before work with the catalog register the new user")
		else:
			self.update_DBjson()
			self.fire.mood(self.db,self.uid,int(time.time()),data)
			return("mood added")

	def GET(self, *uri, **params):  # return parameters
		#return info about registered system service
		if uri[0]=="register":
			self.register(uri[1],uri[2],uri[3])
			return "new id registered"

		elif uri[0]=="delete_actuator":
			self.delete_actuator(uri[1],uri[2])
			return "actuator deleted"
	
		elif uri[0]=="delete_sensor":
			self.delete_sensor(uri[1],uri[2])
			return "sensor deleted"

		elif uri[0]== "delete_user":
			return self.delete_user(uri[1],uri[2])

		elif uri[0]== "delete_event":
			self.delete_event(uri[1],uri[2],uri[3],uri[4])
			return "event deleted"

		elif uri[0] == 'set_forecast_days':
			self.set_forecast_days(int(uri[1]),uri[2])
			return ({uri[1]}, "forecast days set")

		##### Retrieve data ###
		elif uri[0]=="mainbox_name":
			if self.controller==0:
				print("Before work with the catalog register the new user")
			else:
				data = self.update_DBjson()
				#download all database from firebase and upload on the file AMBIENT.JSON after reautenticate
				name = (self.db.get()).val()['Houses'][self.uid]["mainbox"]['name']
				key = (self.db.get()).val()['Houses'][self.uid]["mainbox"]['key']
				dat = json.dumps({'name':name,'key':key})
				#dat = json.loads(dat)
				return dat

		elif uri[0]=="get_weather":
			if self.controller==0:
				print("Before work with the catalog register the new user")
			else:
				data = self.update_DBjson()
				#download all database from firebase and upload on the file AMBIENT.JSON after reautenticate
				appid = (self.db.get()).val()['Houses'][self.uid]["system"][1]['appid']
				lat = (self.db.get()).val()['Houses'][self.uid]["ambient"]['houseconf']['lat']
				lon = (self.db.get()).val()['Houses'][self.uid]["ambient"]['houseconf']['long']
				sunrise = (self.db.get()).val()['Houses'][self.uid]["ambient"]['houseconf']['sunrise']
				sunset = (self.db.get()).val()['Houses'][self.uid]["ambient"]['houseconf']['sunset']
				weather_url= (self.db.get()).val()['Houses'][self.uid]["system"][1]['weather_url']
				forecast_url= (self.db.get()).val()['Houses'][self.uid]["system"][1]['forecast_url']
				forecast_days=(self.db.get()).val()['Houses'][self.uid]["ambient"]['settings']['forecast_days']
				weather_interval= (self.db.get()).val()['Houses'][self.uid]["ambient"]['settings']['weather_interval']
				forecast_interval= (self.db.get()).val()['Houses'][self.uid]["ambient"]['settings']['forecast_interval']			
				dat = json.dumps({'appid':appid,'lat':lat,'lon':lon,'sunrise':sunrise,'sunset':sunset,
				'weather_url':weather_url,'forecast_url':forecast_url,"forecast_days":forecast_days,
				"weather_interval":weather_interval,"forecast_interval":forecast_interval})
				#dat = json.loads(dat)
				return dat

		elif uri[0]=="get_sensors_and_actuators":
			if self.controller==0:
				print("Before work with the catalog register the new user")
			else:
				data = self.update_DBjson()
				#download all database from firebase and upload on the file AMBIENT.JSON after reautenticate
				sensors =({"Sensors":(self.db.get()).val()['Houses'][self.uid]["ambient"]['sensors']}) 
				actuators = ({"Actuators":(self.db.get()).val()['Houses'][self.uid]["ambient"]['actuators']})   
				sensors.update(actuators)
				jsondictA = json.dumps(sensors)
				return jsondictA

		elif uri[0]=="get_mood":
			if self.controller==0:
				print("Before work with the catalog register the new user")
			else:
				data = self.update_DBjson()
				#download all database from firebase and upload on the file AMBIENT.JSON after reautenticate
				housemood =self.db.get().val()['Houses'][self.uid]["ambient"]['housemood']
				jsondictA = json.dumps(housemood)
				return jsondictA

		elif uri[0]=="get_thinghspeak":
			if self.controller==0:
				print("Before work with the catalog register the new user")
			else:
				data = self.update_DBjson()
				#download all database from firebase and upload on the file AMBIENT.JSON after reautenticate
				thinghspeak_api_key =self.db.get().val()['Houses'][self.uid]["system"][2]['api_key']
				thinghspeak_url =self.db.get().val()['Houses'][self.uid]["system"][2]['url']
				thinghspeak_channel =self.db.get().val()['Houses'][self.uid]["system"][2]['channel_id']
				jsondictC = json.dumps({"api_key": thinghspeak_api_key , "url":thinghspeak_url, "channel_id":thinghspeak_channel})
				return jsondictC
		
		elif uri[0]=="get_userslist":
			if self.controller==0:
				print("Before work with the catalog register the new user")
			else:
				data = self.update_DBjson()
				#download all database from firebase and upload on the file AMBIENT.JSON after reautenticate
				known_face_names=[]
				storagelist=[]
				userslist =self.db.get().val()['Houses'][self.uid]["users"]
				for i in range(len(userslist)):
					known_face_names.append(str(userslist[i]['name']))
					storagelist.append(self.fire.getdatastorage(self.st,str(userslist[i]['image'])))
				jsondictC = json.dumps(dict(zip(known_face_names, storagelist)))	
				return jsondictC

		elif uri[0]=="get_telegram":
			if self.controller==0:
				print("Before work with the catalog register the new user")
			else:
				data = self.update_DBjson()
				telegram_token =self.db.get().val()['Houses'][self.uid]["system"][0]['token']
				telegram_url =self.db.get().val()['Houses'][self.uid]["system"][0]['url']
				jsondictC = json.dumps({"token": telegram_token , "url":telegram_url})
				return jsondictC

		else:
			raise cherrypy.HTTPError(400, "operation not available, possible data missing in input")

	def POST(self, *uri, **params):
		if len(uri)!=0:

			if uri[0] == 'sign_up':
				json_string = cherrypy.request.body.read()
				request_obj=json.loads(json_string)
				self.sign_up(request_obj)
				return "signed up"
				
			elif uri[0]=="log_in":
				json_string = cherrypy.request.body.read()
				request_obj=json.loads(json_string)
				self.log_in(request_obj)
				return "logged in"

			elif uri[0]=="create_actuator":
				json_string = cherrypy.request.body.read()
				request_obj=json.loads(json_string)
				self.create_actuator(request_obj)
				return "actuator created"

			elif uri[0]=="create_sensor":
				json_string = cherrypy.request.body.read()
				request_obj=json.loads(json_string)
				self.create_sensor(request_obj)
				return "sensor created"

			elif uri[0]=="create_event":
				json_string = cherrypy.request.body.read()
				request_obj=json.loads(json_string)
				r=self.createchange_event(request_obj)
				return r

			elif uri[0]=="change_actuator":
				json_string = cherrypy.request.body.read()
				request_obj=json.loads(json_string)
				self.change_actuator(request_obj)
				return "actuator changed"

			elif uri[0]=="change_sensor":
				json_string = cherrypy.request.body.read()
				request_obj=json.loads(json_string)
				self.change_sensor(request_obj)
				return "sensor changed"

			elif uri[0]=="upload_forecast":
				json_string = cherrypy.request.body.read()
				request_obj=json.loads(json_string)
				r=self.upload_forecast(request_obj)
				return r

			elif uri[0]=="uploadmood":
				json_string = cherrypy.request.body.read()
				request_obj=json.loads(json_string)
				r=self.upload_mood(request_obj)
				return r

			else:
				raise cherrypy.HTTPError(400, "operation not available, possible spell/data input error")

if __name__ == '__main__':

	conf = {
		'/': {
			'request.dispatch': cherrypy.dispatch.MethodDispatcher(),
			'tools.sessions.on': True
		}
	}
	cherrypy.config.update({'server.socket_host': '0.0.0.0' ,
							'server.socket_port': 8080} )
	cherrypy.tree.mount(AMBIENT_Catalog(), '/',  conf)
	cherrypy.engine.start()
	cherrypy.engine.block()



	###################################CATALOG OPERATIONS##########################################################
#	GET
#	0 register              
#	1 delete_actuator		
#	2 delete_sensor			
#   3 delete user           
#	3 delete_event			
#	4 set_forecast_days		
#	6 add_mainbox_name	    
#	7 mainbox_name	
#   8 get_weather
#	9 get_sensors
#	10 get_mood
#	11 get_thinghspeak
#	12 get_userslist
#	13 get_telegram
#	14 save telegram channel ID

#POST
       
#	1 sign_up			
#	2 log_in			
#	3 create_actuator	
#	4 create_sensor 	
#	5 create_event 		
#	6 change_actuator 	
#	7 change_sensor		
#	8 change_event		
#   9 upload_forecast 
#	10 light acceptance
#	11 time acceptance
#   12 upload mood

##############################################################################################################

