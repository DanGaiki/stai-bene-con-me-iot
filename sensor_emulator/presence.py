import json
import requests
import random
import numpy as np
#import Adafruit_DHT
from threading import Thread
import datetime
import time
import numpy as np
import sys
import dweepy
sys.path.append("../mqtt/")
from MQTT_classes import Publisher


class Presence_sensor(Publisher):
	def __init__(self):
		fp=open('../system/config.JSON','r')
		self.conf=json.load(fp)
		fp.close()
		# loading the address of the Catalog
		self.catalog_ip=self.conf['catalog']['ip']
		self.catalog_port=self.conf['catalog']['port']
		if self.conf['registered']['flag']==1:
			self.controller=1
			self.admin_email=self.conf['admin']['email']        
			self.admin_password=self.conf['admin']['password']
			# catalog acess information about the Main box name to get info about the sensors
			self.catalog_url='http://'+str(self.catalog_ip)+':'+str(self.catalog_port)
			self.mainb = requests.get(str(self.catalog_url +'/mainbox_name/'+str(self.admin_email)+"/"+str(self.admin_password)))
			if (self.mainb.status_code == 200):
				self.mainb_obj = self.mainb.json()
				self.name=self.mainb_obj['name']
				self.appid=self.mainb_obj['key']
				id_string = 'Main_Box/'+ str(self.name)
				super(Presence_sensor,self).__init__(clientID=id_string)

		else:
			pass

	def simulateSensors(self):
		sensors_res = requests.get(str(self.catalog_url +'/get_sensors_and_actuators/'+str(self.admin_email)+"/"+str(self.admin_password)))
		if (sensors_res.status_code == 200):
			raw_dict=sensors_res.json()
		#sensors list
		timenow = (datetime.datetime.today().strftime("%Y-%m-%d %H:%M:%S"))
		topic = 'Control'
		while True:
			for i in range(len(raw_dict["Sensors"])):
				sensor = str("S"+str(i+1))
				d={'sensor':sensor,'value':'on'}
				message = json.dumps(d)
				               #message1 = json.loads(message)
				self.mqtt_publish(topic=topic, message=message)
				print("-->transmitted topic", topic)
				print("-->transmitted message", message)
				time.sleep(20)
				d2={'sensor':sensor,'value':'off'}
				message2 = json.dumps(d2)
				#message3 = json.loads(message2)
				self.mqtt_publish(topic=topic, message=message2)
				print("-->transmitted topic", topic)
				print("-->transmitted message",message2)
				time.sleep(10)

	def mqtt_start(self):
		Publisher.mqtt_start(self)

if __name__ == '__main__':
	mb=Presence_sensor()
	#if mb==True:
	mb.mqtt_start()
	mb.simulateSensors()
	time.sleep(3)
	mb.mqtt_stop()
	#else:
		#print("user not registerd Yet")
		#mb = Presence_sensor()
