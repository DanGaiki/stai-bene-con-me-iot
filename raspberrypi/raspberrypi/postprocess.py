import json
import string
import matplotlib.pyplot as plt
from pandas.plotting import register_matplotlib_converters
register_matplotlib_converters()
from pylab import plot, ylim, xlim, show, xlabel, ylabel, grid
from numpy import linspace, loadtxt, ones, convolve
import numpy as numpy
import sys
import requests
from MQTT_classes import Publisher

class PostProcess(Publisher):
	def __init__(self):
		fp=open('config.JSON','r')
		self.conf=json.load(fp)
		fp.close()
		# loading the address of the Catalog
		self.catalog_ip=self.conf['catalog']['ip']
		self.catalog_port=self.conf['catalog']['port']
		self.admin_email=self.conf['admin']['email']         # this is a child solution, how to use real cryptography
		self.admin_password=self.conf['admin']['password']
		# catalog acess information about the Main box name to get info about the sensors
		self.catalog_url='http://'+str(self.catalog_ip)+':'+str(self.catalog_port)
		self.mainb = requests.get(str(self.catalog_url +'/mainbox_name/'+str(self.admin_email)+"/"+str(self.admin_password)))
		if (self.mainb.status_code == 200):
			self.mainb_obj = self.mainb.json()
		self.name=self.mainb_obj['name']
		self.appid=self.mainb_obj['key']
		id_string = 'Main_Box/'+ str(self.name)
		super(PostProcess,self).__init__(clientID=id_string)

	def calc(self,moodfile):
		file = moodfile
		with open(file) as train_file:
			dict_train = json.load(train_file)
		train_file.close()
		#jsonStr = dict_train.decode("utf-8")
		j=json.loads(dict_train)
		x = j["time"]
		a = j['Disgust']
		b = j['Scared']
		c = j['Happy']
		d = j['Sad']
		e = j['Surprised']
		f = j['Neutral']
		g = j['Angry']
		user=j['username']
		datetime=j['datetime']
		mood_names=['Disgust','Scared','Happy','Sad','Surprised','Neutral','Angry']
		th=0.2
		cta=float(sum(1 for item in a if item>(th)))
		ctb=float(sum(1 for item in b if item>(th)))
		ctc=float(sum(1 for item in c if item>(th)))
		ctd=float(sum(1 for item in d if item>(th)))
		cte=float(sum(1 for item in e if item>(th)))
		ctf=float(sum(1 for item in f if item>(th)))
		ctg=float(sum(1 for item in g if item>(th)))
		counta=float(sum(item for item in a if item>(th)))/float(len(j["time"]))
		countb=float(sum(item for item in b if item>(th)))/float(len(j["time"]))
		countc=float(sum(item for item in c if item>(th)))/float(len(j["time"]))
		countd=float(sum(item for item in d if item>(th)))/float(len(j["time"]))
		counte=float(sum(item for item in e if item>(th)))/float(len(j["time"]))
		countf=float(sum(item for item in f if item>(th)))/float(len(j["time"]))
		countg=float(sum(item for item in g if item>(th)))/float(len(j["time"]))
		performance = [counta,countb,countc,countd,counte,countf,countg]
		NUM=(performance.index(max(performance)))
		moodstatus=(mood_names[NUM])
		listaa=[]
		for i in range(len(performance)):
			if i== NUM:
				listaa.append(1)
			listaa.append(0)
		print("-->Detected mood Status is:",moodstatus)	
		return performance,listaa,user,datetime,moodstatus

	def plots(self,performance,username,datetime):
		#Barchart
		objects = ('Disgust', 'Scared', 'Happy', 'Sad', 'Surprised', 'Neutral','Angry')
		colors= ("orange","mediumblue", "green", "crimson", "purple","yellow","red")
		y_pos = numpy.arange(len(objects))
		plt.bar(y_pos, performance, align='center', alpha=0.5,color=colors)
		plt.xticks(y_pos, objects)
		plt.ylabel('Counts/total')
		plt.title (username + ' - mood State at ' + datetime)
		figname= str(datetime)+ ".jpg"
		plt.savefig('database/'+figname)  

	#Change this by post method on catalog
	def uploadfirebase(self,listaa,sensornumber,user,moodstatus):  #upload a graph image in the firebase
		data=json.dumps({"sensor":sensornumber,
		'mood':{'Disgust':listaa[0],'Scared':listaa[1],'Happy':listaa[2],
		'Sad':listaa[3],'Surprised':listaa[4],'Neutral':listaa[5],'Angry': listaa[6]},
		'username':user})
		mood = requests.post(str(self.catalog_url +'/uploadmood/'+str(self.admin_email)+"/"+str(self.admin_password)),data=data)
		self.mqtt_start()
		self.mqtt_publish(topic='Control', message=data)
		self.mqtt_stop()		
		#print ("transmitted message from postprocess",testando)

	def mqtt_start(self):
		Publisher.mqtt_start(self)