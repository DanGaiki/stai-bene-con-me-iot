from keras.preprocessing.image import img_to_array
import imutils
import cv2
from keras.models import load_model
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import time
from itertools import count
import random
import json
from matplotlib.ticker import AutoMinorLocator, MultipleLocator, FuncFormatter
import datetime
import face_recognition
import sys
import requests
import os
import urllib
os.environ['TF_CPP_MIN_LOG_LEVEL']='3'
from postprocess import PostProcess

class Moodrecognition(object):

	def __init__(self,operation,browsetest):
#JSON DATA
		self.operation=operation
		self.browsetest=browsetest
# parameters for loading data and images
		self.detection_model_path = 'haarcascade_files/haarcascade_frontalface_default.xml'
		self.emotion_model_path = 'models/_mini_XCEPTION.102-0.66.hdf5'
		self.roilist=[]
# hyper-parameters for bounding boxes shape
# loading models
		self.face_detection = cv2.CascadeClassifier(self.detection_model_path)
		self.emotion_classifier = load_model(self.emotion_model_path, compile=False)
		self.EMOTIONS = ["Angry" ,"Disgust","Scared", "Happy", "Sad", "Surprised", "Neutral"]
# initialise lists to save the times 
		self.angry=[]
		self.disgust=[]
		self.scared=[]
		self.happy=[]
		self.sad=[]
		self.surprised=[]
		self.neutral=[]
		self.feelings_faces = []
		self.index = count()
		self.x_vals=[]
		self.t0 = time.clock()
#for index, emotion in enumerate(EMOTIONS):
   #feelings_faces.append(cv2.imread('emojis/' + emotion + '.png', -1))
# starting video streaming
		#cv2.namedWindow('Moodiot')
		if (operation == 'test'):
			self.camera = cv2.VideoCapture(str(self.browsetest))#If desire to use one of the our amazing test videos and show the beautiful faces
			print("operation mode = video test")
		if (operation == 'camera'):
			self.camera = cv2.VideoCapture(0)
			print("operation mode = raspberrypi camera")
		# If desire to use the raspberrypi camera
		self.camera.set(3,640) #set Width
		self.camera.set(4,480) #set Height
		self.datetim=str(datetime.datetime.now().strftime("%H:%M:%S"))
		#load users registered from firebase database from catalog 
		fp=open('config.JSON','r')
		self.conf=json.load(fp)
		fp.close()
		# loading the address of the Catalog
		self.catalog_ip=self.conf['catalog']['ip']
		self.catalog_port=self.conf['catalog']['port']
		self.admin_email=self.conf['admin']['email']         
		self.admin_password=self.conf['admin']['password']
		# catalog acess
		self.catalog_url='http://'+str(self.catalog_ip)+':'+str(self.catalog_port)
		# MQTT override, get information about the id on the catalog
		self.mainb = requests.get(str(self.catalog_url +'/mainbox_name/'+str(self.admin_email)+"/"+str(self.admin_password)))
		if (self.mainb.status_code == 200):
			self.mainb_obj = self.mainb.json()
		self.name=self.mainb_obj['name']
		self.appid=self.mainb_obj['key']
		self.known_face_names=[]
		self.storagelist=[]

		#from catalog get the list of users
		users_res = requests.get(str(self.catalog_url +'/get_userslist/'+str(self.admin_email)+"/"+str(self.admin_password)))
		if (users_res.status_code == 200):
			self.dictionary=users_res.json()
		self.known_face_names=list(self.dictionary.keys())
		self.known_face_encodings=[]
		#print(self.dictionary[self.known_face_names[1]])
		for i in range(len(self.dictionary)):
			print("Searching the face",str(self.known_face_names[i]))
			resp = urllib.request.urlopen(str(self.dictionary[self.known_face_names[i]])) 
			#resp = urllib.urlopen(dictionary[self.known_face_names[i]])
			image = np.asarray(bytearray(resp.read()), dtype="uint8")
			image = cv2.imdecode(image, cv2.IMREAD_COLOR)
			self.known_face_encodings.append(face_recognition.face_encodings(image)[0])
			self.flag=True

	def classificatmood(self,sensornumber):
		count=0
		while (self.camera.isOpened()):
			if time.clock() - self.t0 >15:
				break
			ret =self.camera.read()[0]
			frame = self.camera.read()[1]
			#frame = cv2.flip(frame, -1) # Flip camera vertically
			if ret==True:
				#reading the frame
				frame = imutils.resize(frame,width=300)
				gray = cv2.cvtColor(frame, cv2.COLOR_BGR2GRAY)
				faces = self.face_detection.detectMultiScale(gray,scaleFactor=1.1,minNeighbors=5,minSize=(30,30),flags=cv2.CASCADE_SCALE_IMAGE)
				canvas = np.zeros((250, 300, 3), dtype="uint8")
				frameClone = frame.copy()
				if len(face_recognition.face_encodings((frame))) > 0 and self.flag:
					self.flag = False
					self.unknown_face_encoding = face_recognition.face_encodings((frame))[0]
				if len(faces) > 0 :
					faces = sorted(faces, reverse=True,
					key=lambda x: (x[2] - x[0]) * (x[3] - x[1]))[0]
					(fX, fY, fW, fH) = faces
		# Extract the ROI of the face from the grayscale image, resize it to a fixed 28x28 pixels, and then prepare
		# the ROI for classification via the CNN
					roi = gray[fY:fY + fH, fX:fX + fW]
					roi = cv2.resize(roi, (64, 64))
					roi = roi.astype("float") / 255.0
					roi = img_to_array(roi)
					roi = np.expand_dims(roi, axis=0)
					self.roilist.append(roi)
					#preds = self.emotion_classifier.predict(roi)[0]
					#emotion_probability = np.max(preds)
					#label = self.EMOTIONS[preds.argmax()]
					count +=1
				else: continue
			if count>=11 or (time.clock() - self.t0) >17:
				self.camera.release()
				cv2.destroyAllWindows()
				random.shuffle(self.roilist)
				break
				
		if 	len(self.roilist) > 5:	
			for i in range (3,11):
				print ("Analising video frame :",i)
				preds = self.emotion_classifier.predict(roi)[0]
				#preds = self.emotion_classifier.predict(roi)[0]
				emotion_probability = np.max(preds)
				label = self.EMOTIONS[preds.argmax()]
				for (i, (emotion, prob)) in enumerate(zip(self.EMOTIONS, preds)):
							# construct the label text
							text = "{}: {:.2f}%".format(emotion, prob * 100)
							if emotion =="Angry":
								if prob <0.01:
									prob =0
								self.angry.append(format(prob, '.3f'))
								self.disgust.append(0)
								self.scared.append(0)
								self.happy.append(0)
								self.sad.append(0)
								self.surprised.append(0)
								self.neutral.append(0)

							if emotion =="Disgust":
								if prob <0.01:
									prob =0
								self.disgust.append(format(prob, '.3f'))
								self.angry.append(0)
								self.scared.append(0)
								self.happy.append(0)
								self.sad.append(0)
								self.surprised.append(0)
								self.neutral.append(0)
	 
							if emotion =="Scared":
								if prob <0.01:
									prob =0
								self.scared.append(format(prob, '.3f'))
								self.angry.append(0)
								self.disgust.append(0)
								self.happy.append(0)
								self.sad.append(0)
								self.surprised.append(0)
								self.neutral.append(0)

							if emotion =="Happy":
								if prob <0.01:
									prob =0
								self.happy.append(format(prob, '.3f'))  
								self.angry.append(0)
								self.disgust.append(0)
								self.scared.append(0)
								self.sad.append(0)
								self.surprised.append(0)
								self.neutral.append(0)

							if emotion =="Sad":
								if prob <0.01:
									prob =0
								self.sad.append(format(prob, '.3f')) 
								self.angry.append(0)
								self.disgust.append(0)
								self.scared.append(0)
								self.happy.append(0)
								self.surprised.append(0)
								self.neutral.append(0)

							if emotion =="Surprised":
								if prob <0.01:
									prob =0
								self.surprised.append(format(prob, '.3f'))
								self.angry.append(0)
								self.disgust.append(0)
								self.scared.append(0)
								self.happy.append(0)
								self.sad.append(0)
								self.neutral.append(0)

							if emotion =="Neutral":
								if prob <0.01:
									prob =0
								self.neutral.append(format(prob, '.3f'))
								self.angry.append(0)
								self.disgust.append(0)
								self.scared.append(0)
								self.happy.append(0)
								self.sad.append(0)
								self.surprised.append(0)
							self.x_vals.append(next(self.index))
							
			compare=np.asarray(self.unknown_face_encoding)
			for i in range(len(self.dictionary)):
				results = face_recognition.compare_faces([np.asarray(self.known_face_encodings[i])], compare)
				if results[0]:
					print("Recognized User : "+ (self.known_face_names[i]))
					self.user=(self.known_face_names[i])
				else:
					print("This is NOT :" + (self.known_face_names[i]))

			self.angry=np.array(self.angry,dtype=float)
			self.disgust=np.array(self.disgust,dtype=float) 
			self.scared=np.array(self.scared,dtype=float)
			self.happy=np.array(self.happy,dtype=float)
			self.sad=np.array(self.sad,dtype=float)
			self.surprised=np.array(self.surprised,dtype=float)
			self.neutral=np.array(self.neutral,dtype=float)

			b = json.dumps({'Angry': self.angry.tolist(),
			'Disgust':self.disgust.tolist(),
			'Scared':self.scared.tolist(),
			'Happy':self.happy.tolist(),
			'Sad':self.sad.tolist(),
			'Surprised':self.surprised.tolist(),
			'Neutral':self.neutral.tolist(),
			'time':self.x_vals,
			'username':self.user,
			'datetime':self.datetim
			})
			self.camera.release()
			cv2.destroyAllWindows()
			timestamp = int(time.time())
			MOOD_file='database/'+ str(timestamp) + '.json'
			ff=open(MOOD_file,'w+')
			json.dump(b,ff)
			ff.close()
			pp=PostProcess()
			print("Post process starting...")
			perf,listaa,userr,datatim,moodsts=pp.calc(MOOD_file)
			pp.plots(perf,userr,str(timestamp))
			pp.uploadfirebase(listaa,sensornumber,userr,moodsts)
			return ("Process done")
		else:
			self.camera.release()
			cv2.destroyAllWindows()
			return False
