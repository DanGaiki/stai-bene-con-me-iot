import paho.mqtt.client as PahoMQTT

class Publisher(object):
	def __init__(self, clientID):

		self.clientID = clientID
		self._paho_mqtt = PahoMQTT.Client(protocol=PahoMQTT.MQTTv311)
		self._paho_mqtt.on_connect = self.mqtt_onConnect
		self.broker='mqtt.eclipse.org'
		self.port=1883

	def mqtt_start (self):
		#manage connection to broker
		self._paho_mqtt.connect(self.broker, self.port)
		self._paho_mqtt.loop_start()

	def mqtt_stop (self):
		self._paho_mqtt.loop_stop()
		self._paho_mqtt.disconnect()

	def mqtt_onConnect(self, paho_mqtt, userdata, flags, rc):
		print ("%s connected to message broker with result code: %s" %(self.clientID,str(rc)))

	def mqtt_publish (self, topic, message):
		# publish a message with a certain topic
		self._paho_mqtt.publish(topic, message, 2)
		print ("%s: Message published by with topic %s"%(self.clientID,topic))

class Subscriber(object):

	def __init__(self,clientID,sub_topic):
		self.clientID = clientID
		self._paho_mqtt = PahoMQTT.Client(protocol=PahoMQTT.MQTTv311) 
		self._paho_mqtt.on_connect = self.mqtt_onConnect
		self._paho_mqtt.on_message = self.mqtt_onMessageReceived
		self.broker='mqtt.eclipse.org'
		self.port=1883
		self.sub_topic = sub_topic

	def mqtt_start (self):
		#manage connection to broker
		self._paho_mqtt.connect(self.broker, self.port)
		self._paho_mqtt.loop_start()
		# subscribe for a topic
		self._paho_mqtt.subscribe(self.sub_topic, 2)
		print ("subscribed to topic %s" %self.sub_topic)

	def mqtt_stop (self):
		self._paho_mqtt.loop_stop()
		self._paho_mqtt.disconnect()

	def mqtt_onConnect (self, paho_mqtt, userdata, flags, rc):
		print ("%s connected to message broker with result code: %s" %(self.clientID,str(rc)))

	def mqtt_onMessageReceived (self, paho_mqtt , userdata, msg):
		print ('%s: Message received with topic %s' %(self.clientID,msg.topic))

class PublisherSubscriber(object):

	def __init__(self, clientID, sub_topic):
		self.clientID = clientID
		self._paho_mqtt = PahoMQTT.Client(protocol=PahoMQTT.MQTTv311)
		self._paho_mqtt.on_connect = self.mqtt_onConnect
		self._paho_mqtt.on_message = self.mqtt_onMessageReceived
		self.broker='mqtt.eclipse.org'
		self.port=1883
		self.sub_topic = sub_topic

	def mqtt_start (self):
		#manage connection to broker
		self._paho_mqtt.connect(self.broker, self.port)
		self._paho_mqtt.loop_start()
		# subscribe for a topic
		self._paho_mqtt.subscribe(self.sub_topic)

	def mqtt_stop (self):
		self._paho_mqtt.loop_stop()
		self._paho_mqtt.disconnect()

	def mqtt_onConnect (self, paho_mqtt, userdata, flags, rc):
		print ("%s connected to message broker with result code: %s" %(self.clientID,str(rc)))

	def mqtt_onMessageReceived (self, paho_mqtt , userdata, msg):
		print ('%s: Message received with topic %s' %(self.clientID,msg.topic))

	def mqtt_publish (self, topic, message):
		# publish a message with a certain topic
		self._paho_mqtt.publish(topic, message, 2)
		print ("%s: Message published by with topic %s"%(self.clientID,topic))
