face-recognition-models==0.3.0
tensorflow==2.2.0rc1
CherryPy==18.5.0
matplotlib==2.1.1
Keras==2.1.2
opencv-python==4.2.0.32
numpy==1.18.2
paho-mqtt==1.5.0 
requests==2.21.0
scipy>=0.17.0
Pillow
Click>=6.0







