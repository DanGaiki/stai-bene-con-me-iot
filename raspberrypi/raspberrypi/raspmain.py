import json
import time
import requests
import random
import numpy as np
#import Adafruit_DHT
from threading import Thread
import datetime
import time
import numpy as np
import sys
from moodrecognition import Moodrecognition
from MQTT_classes import PublisherSubscriber

class Main_Box(PublisherSubscriber):
	def __init__(self):
		fp=open('config.JSON','r')
		self.conf=json.load(fp)
		fp.close()
		# loading the address of the Catalog
		self.catalog_ip=self.conf['catalog']['ip']
		self.catalog_port=self.conf['catalog']['port']
		self.admin_email=self.conf['admin']['email']         
		self.admin_password=self.conf['admin']['password']
		# catalog acess
		self.catalog_url='http://'+str(self.catalog_ip)+':'+str(self.catalog_port)
		# MQTT override, get information about the id on the catalog
		self.mainb = requests.get(str(self.catalog_url +'/mainbox_name/'+str(self.admin_email)+"/"+str(self.admin_password)))
		if (self.mainb.status_code == 200):
			self.mainb_obj = self.mainb.json()
		self.name=self.mainb_obj['name']
		self.appid=self.mainb_obj['key']
		id_string = 'Main_Box/'+ str(self.name)
		#self.subscribed_topic=str(self.name) + '/#'
		self.subscribed_topic= 'Control/#'
		super(Main_Box,self).__init__(clientID=id_string,sub_topic=self.subscribed_topic)

		#option by catalog address 
		self.weath = requests.get(str(self.catalog_url +'/get_weather/'+str(self.admin_email)+"/"+str(self.admin_password)))
		if (self.mainb.status_code == 200):
			self.weath_obj=self.weath.json()
		self.appidw=self.weath_obj['appid']
		self.lat=self.weath_obj['lat']
		self.lon=self.weath_obj['lon']
		self.sunrise=self.weath_obj['sunrise']
		self.sunset=self.weath_obj['sunset']
		self.forecast_days=self.weath_obj['forecast_days']
		self.weather_url=self.weath_obj['weather_url'].format(self.lat,self.lon,self.appidw)
		self.forecast_url=self.weath_obj['forecast_url'].format(self.lat,self.lon,self.appidw)
		self.weather_interval=self.weath_obj['weather_interval']
		self.forecast_interval=self.weath_obj['forecast_interval']

	#real sensor DHT22
	#def realSensor(self):
		#sensor = Adafruit_DHT.DHT22
		#pin = 2
		#humidity, temperature = Adafruit_DHT.read_retry(sensor, pin)
		#return humidity, temperature

	def simulateSensors(self):
		temp,humidity,sunrise,sunset=self.http_getClimate()
		#humidity,temp=self.realSensor()
		timestamp=time.time()
		senML={}
		senML['bn']=self.clientID
		senML['e']=[]
		senML['e'].append({'n': 'temperature', 'u': "degC", 't': timestamp, "v":temp})
		senML['e'].append({'n': 'humidity', 'u': "%", 't': timestamp, "v":humidity})
		message=json.dumps(senML)
		self.mqtt_publish(topic='climate', message=message)
		timenow = (datetime.datetime.today().strftime("%Y-%m-%d %H:%M:%S"))
		return ("done")

	def http_getClimate(self):
		clima = requests.get(str(self.weather_url))
		if (clima.status_code == 200):
			raw_dict=clima.json()
			self.temperature=round(raw_dict['main']['temp']-273.15,2)
			self.humidity=raw_dict['main']['humidity']
			sunrise=raw_dict['sys']['sunrise']
			sunset=raw_dict['sys']['sunset']
			return self.temperature,self.humidity,sunrise,sunset
		else:
			return "error"

	def http_getForecast(self):
		now = datetime.datetime.now()
		current_time =  now.strftime("%Y-%m-%d %H:%M:%S")
		forecast = requests.get(str(self.forecast_url))
		if (forecast.status_code == 200):
			raw_dict=forecast.json()
			#local lists for forecast
			forecast_time=[]
			forecast_description=[]
			forecast_temp=[]
			forecast_temp_max=[]
			forecast_temp_min=[]
			forecast_fells_like=[]
			#local lists after separate for time interval
			forecast_timeP=[]
			forecast_descriptionP=[]
			forecast_tempP=[]
			forecast_temp_maxP=[]
			forecast_temp_minP=[]
			forecast_fells_likeP=[]
			#inserted on the firebase

			for i in range(len(raw_dict['list'])):
				tim=(datetime.datetime.strptime((raw_dict['list'][i]['dt_txt']), "%Y-%m-%d %H:%M:%S"))
				forecast_time.append(tim)
				forecast_description.append(str(raw_dict['list'][i]['weather'][0]["description"]))
				forecast_temp.append("{0:.2f}".format((float(raw_dict['list'][i]['main']['temp']/10))))
				forecast_temp_max.append("{0:.2f}".format((float(raw_dict['list'][i]['main']['temp_max']/10))))
				forecast_temp_min.append("{0:.2f}".format((float(raw_dict['list'][i]['main']['temp_min']/10))))
				forecast_fells_like.append("{0:.2f}".format((float(raw_dict['list'][i]['main']['feels_like']/10))))

			#define time delta forecast days
			date_1 = datetime.datetime.strptime(current_time, "%Y-%m-%d %H:%M:%S")
			end_date = date_1 + datetime.timedelta(days=int(self.forecast_days))

			#create a forecast period list
			for i in range(len(forecast_time)):
				if forecast_time[i] < end_date:
					forecast_timeP.append(str(forecast_time[i]))
					forecast_descriptionP.append(forecast_description[i])
					forecast_tempP.append((forecast_temp[i]))
					forecast_temp_maxP.append(forecast_temp_max[i])
					forecast_temp_minP.append(forecast_temp_min[i])
					forecast_fells_likeP.append(forecast_fells_like[i])

			#upload the last forecast in the catalog
			for i in range(len(forecast_timeP)):
				data= json.dumps({forecast_timeP[i]:{"forecast_description" : forecast_descriptionP[i] ,"forecast_fells_like": forecast_fells_likeP[i],
				"forecast_temp": forecast_timeP[i],"forecast_temp_max" : forecast_temp_maxP[i],"forecast_temp_min" : forecast_temp_minP[i],
				"forecast_fells_like" : forecast_fells_likeP[i]}}).encode('utf8')
				req=requests.post(str(self.catalog_url +'/upload_forecast/'+str(self.admin_email)+"/"+str(self.admin_password)), data=data)

		else:
			return "error"

	def mqtt_start(self):
		PublisherSubscriber.mqtt_start(self)

	def sensor_routine(self, t_end):
		while time.time()<t_end:
			self.simulateSensors()
			time.sleep(int(self.weather_interval))

	def forecast_routine(self, t_end):
		while time.time()<t_end:
			self.http_getForecast()
			time.sleep(int(self.forecast_interval))

	def mqtt_onMessageReceived(self, paho_mqtt, userdata, msg):
		self.completeMeasurement(msg.payload)

	def completeMeasurement(self,mensagem):
		# received messages can be Sensor or Mood.
		control=json.loads(mensagem)

		if len(control) ==2:
			control_sensor=str(control["sensor"])
			control_value=str(control["value"])
			print("--->Received sensor message",control_sensor,control_value)
			if control_value == "on":
				print("Detecting the MOOD")
				#browsetest= random.choice(listavideos)
				browsetest="/home/ddd/Documents/testvideos/"+str(random.randint(1,9)) +".mp4"
				mood=Moodrecognition("test",browsetest) #change "camera" to test
				plot=mood.classificatmood(control_sensor)
			if control_value == "off":
				print("Sensor switching OFF")
			else:
				pass
		else:
			control_sensor_mood=str(control["sensor"])
			control_mood=(control["mood"])
			control_username=str(control["username"])
			moodlist=['Scared','Sad','Neutral','Disgust','Angry', 'Surprised','Happy']
			for i in range(len(control_mood)):
				if control_mood[moodlist[i]]==1:
					moodstatus=str(moodlist[i])
			topic= 'Thinghspeak'
			message=json.dumps({"mood":str(moodstatus),"Temperature":str(self.temperature),"Humidity":str(self.humidity)})
			print("-->Publishing the message : ",message)
			self.mqtt_publish(topic=topic, message=message)	
			topic1= 'Telegram'
			self.mqtt_publish(topic=topic1, message=message)

	def global_routine(self):
		self.mqtt_start()
		t_end = time.time() + 3600
		thread_w = Thread(target = self.sensor_routine, args=[t_end])
		thread_f = Thread(target = self.forecast_routine, args=[t_end])
		thread_w.start()
		thread_f.start()
		thread_w.join()
		thread_f.join()
		self.mqtt_stop()
	
if __name__ == '__main__':
	mb=Main_Box()
	mb.global_routine()
